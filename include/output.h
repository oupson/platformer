#ifndef PLATFORMER_OUTPUT_H
#define PLATFORMER_OUTPUT_H

#include <SDL2/SDL.h>

struct Output {
    int width, height;
    SDL_Window* window;
    SDL_Renderer* renderer;
};

typedef struct Output Output;

Output* create_output();
void destroy_output(Output *output);

#endif //PLATFORMER_OUTPUT_H