#ifndef PLATFORMER_GAME_H
#define PLATFORMER_GAME_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include <math.h>

#include "input.h"
#include "output.h"
#include "character.h"


struct Game {
    Input *input;
    Output *output;

    int floorY;
    Character character;
};

typedef struct Game Game;

Game* create_game();
void tick_game(Game *game);
void render_game(Game *game);
void run_game(Game *game);
void quit_game(Game *game);

#endif //PLATFORMER_GAME_H
