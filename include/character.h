#ifndef PLATFORMER_CHARACTER_H
#define PLATFORMER_CHARACTER_H

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include "input.h"
#include "output.h"

struct Character {
    double x, y;
    double vx, vy;
    int w, h;
};

typedef struct Character Character;

struct Game;
typedef struct Game Game;

void create_character(Character *character, double x, double y);
void tick_character(Character *character, int floorY, Input *input);
void render_character(Character *character, Output *output);
void destroy_character(Character *character);

#endif //PLATFORMER_CHARACTER_H