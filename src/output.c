#include "output.h"

Output* create_output() {
    Output *output = (Output*) malloc(sizeof(Output));

    SDL_DisplayMode dm;
    SDL_GetDesktopDisplayMode(0, &dm);
    output->width = dm.w;
    output->height = dm.h;
    output->window = SDL_CreateWindow("SDL Platformer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, output->width, output->height, SDL_WINDOW_FULLSCREEN_DESKTOP);
    output->renderer = SDL_CreateRenderer(output->window, -1, 0);

    return output;
}

void destroy_output(Output *output) {
    SDL_DestroyRenderer(output->renderer);
    SDL_DestroyWindow(output->window);

    free(output);
}